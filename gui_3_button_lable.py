"""
时间：2021/03/08
版本号：1.2
重点：按钮Button,标签Lable
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("500x400")

var = tk.StringVar()

l = tk.Label(root,textvariable=var,bg='green',font=('Arial,12'),width=15,
             height=2)  # 标签 用于显示不可编辑的文本或图标
l.pack()    # 放置 放在上下左右

on_hit=False
def hit_me():
    global on_hit
    if on_hit==False:
        on_hit=True
        var.set("you hit me!")
    else:
        on_hit=False
        var.set("")

b = tk.Button(root,text='hit_me',
              bg='white',font=("Arial,12"),width=15,height=2,
              command=hit_me)     # 给窗口添加按钮
b.pack()

root.mainloop()     # 大型的while循环
