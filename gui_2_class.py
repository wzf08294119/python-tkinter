"""
测试一个经典的gui程序写法，用面向对象的方式
日期：2021/03/05
版本：1.1
"""
from tkinter import *
from tkinter import messagebox


class Application(Frame):
    # 一个经典的gui程序类的写法

    def __init__(self, master=None):
        super().__init__(master)
        self.master = master    # 初始化属性
        self.pack()
        self.createWidget()

    def createWidget(self):

        # 创建组件
        self.bt01 = Button(self)
        self.bt01["text"] = "点击送花"
        self.bt01.pack()
        self.bt01["command"] = self.songhua

        # 创建一个推出按钮
        self.btQuit = Button(self,text='退出',command=root.destroy)
        self.btQuit.pack()

    def songhua(self):
        messagebox.showinfo("送花","送你99朵玫瑰花")


if __name__ == '__main__':
    root = Tk()
    root.geometry("400x100+200+300")
    root.title('一个经典的gui测试程序')
    app = Application(master=root)

    root.mainloop()
