import tkinter as tk
root = tk.Tk()
labels = [['1','2','3'], # 文本，布局为网格
          ['4','5','6'],
          ['7','8','9'],
          ['*','0','#']]
for r in range(4):
    for c in range(3):
        l = tk.Label(root,relief = tk.RAISED,padx=20,text=labels[r][c])
        l.grid(row=r,column = c)

root.title('grid测试')
root.mainloop()