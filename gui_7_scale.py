"""
时间：2021/03/10
版本号：1.6
重点：scale拉动条
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("500x400")

l = tk.Label(root,bg='green',width=25,
             text="empty")
l.pack()

def print_selection(v):
    l.config(text="you have selected" + v)

sc = tk.Scale(root,label='try me',from_=0,to=10,
              orient=tk.HORIZONTAL,length=200,
              showvalue=0,tickinterval=2,resolution=0.1,
              command=print_selection)
sc.pack()

root.mainloop()     # 大型的while循环



