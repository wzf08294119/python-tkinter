#在屏幕上创建一块矩形区域,多作为容器来布局窗体
import tkinter as  tk
root = tk.Tk()
root.title("hello world")
root.geometry('300x200')

tk.Label(root, text='校训', font=('Arial', 20),relief = tk.RAISED).pack()

frm = tk.Frame(root)
#left
frm_L = tk.Frame(frm)
tk.Label(frm_L, text='厚德', font=('Arial', 15),relief = tk.RAISED).pack(side=tk.TOP)
tk.Label(frm_L, text='博学', font=('Arial', 15),relief = tk.RAISED).pack(side=tk.TOP)
frm_L.pack(side=tk.LEFT)

#right
frm_R = tk.Frame(frm)
tk.Label(frm_R, text='敬业', font=('Arial', 15),relief = tk.RAISED).pack(side=tk.TOP)
tk.Label(frm_R, text='乐群', font=('Arial', 15),relief = tk.RAISED).pack(side=tk.TOP)
frm_R.pack(side=tk.RIGHT)

frm.pack()

root.mainloop()