"""
时间：2021/03/09
版本号：1.3
重点：窗口用于输入-Entry ，窗口输出界面-Text
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("500x400")

e = tk.Entry(root,show=None)
e.pack()

def insert_point():
    var = e.get()
    t.insert("insert",var)

def insert_end():
    var = e.get()
    t.insert("end",var)

b1 = tk.Button(root,text='insert point',
              bg='white',font=("Arial,12"),
              width=15,height=1,command=insert_point)     # 给窗口添加按钮
b1.pack()

b2 = tk.Button(root,text="insert end",
               command=insert_end)
b2.pack()

t = tk.Text(root,height=2)
t.pack()

root.mainloop()     # 大型的while循环




