"""
日期：2021/03/05
版本号：1.0
"""

from tkinter import *  # 可以直接调用函数
from tkinter import messagebox  # 引入指定的函数

root = Tk()

root.title("我的第一个gui程序")
root.geometry("500x300+100+200")
btn01 = Button(root)
btn01["text"] = "点我就送小红花"
btn01.pack()


def songhua(e):
    messagebox.showinfo("message","送你一朵玫瑰花！")
    print("送你玫瑰")


btn01.bind("<Button-1>", songhua)
root.mainloop()  # 调用组件 进入事件循环
