"""
时间：2021/03/10
版本号：1.5
重点：Radiobutton可点选的按钮
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("500x400")

var = tk.StringVar()
l = tk.Label(root,bg='green',width=25,
             text="empty")
l.pack()

def print_selection():
   l.config(text="you have selected" + var.get())

r1 = tk.Radiobutton(root,text='OptionA',
                    variable=var,value="A",
                    command=print_selection)
r1.pack()

r2 = tk.Radiobutton(root,text='OptionB',
                    variable=var,value="B",
                    command=print_selection)
r2.pack()

r3 = tk.Radiobutton(root,text='OptionC',
                    variable=var,value="C",
                    command=print_selection)
r3.pack()

root.mainloop()     # 大型的while循环



