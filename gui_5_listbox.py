"""
时间：2021/03/09
版本号：1.4
重点：listbox列表部件
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("500x400")

var1 = tk.StringVar()
l = tk.Label(root,bg='green',width=4,
             textvariable=var1)
l.pack()


var2 = tk.StringVar()
var2.set((11,22,33,44))
lb = tk.Listbox(root,listvariable=var2)
list_items = [1,2,3,4]
for item in list_items:
    lb.insert("end",item)
lb.insert(1,'first')
lb.insert(2,'secend')
lb.delete(2)
lb.pack()


def print_select():
    value = lb.get(lb.curselection())
    var1.set(value)


b1 = tk.Button(root,text='print selection',
              bg='white',font=("Arial,12"),
              width=15,height=1,command=print_select)     # 给窗口添加按钮
b1.pack()


t = tk.Text(root,height=2)
t.pack()

root.mainloop()     # 大型的while循环




