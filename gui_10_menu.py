"""
时间：2021/03/10
版本号：2.0
重点：Menu 菜单
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("300x200")

lb = tk.Label(root,text='   ', bg='yellow')
lb.pack()

count = 0


def do_job():
    global count
    lb.config(text='do'+str(count))
    count += 1


menu = tk.Menu(root)

file_menu = tk.Menu(menu, tearoff=0)
menu.add_cascade(label='File', menu=file_menu)
file_menu.add_cascade(label='New', command=do_job)
file_menu.add_cascade(label='Open', command=do_job)
file_menu.add_cascade(label='Save', command=do_job)
file_menu.add_cascade(label='Delete', command=do_job)
file_menu.add_separator()
file_menu.add_cascade(label='Exit', command=root.quit)

edit_menu = tk.Menu(menu, tearoff=0)
menu.add_cascade(label='Edit', menu=edit_menu)
edit_menu.add_cascade(label='Cut', command=do_job)
edit_menu.add_cascade(label='Copy', command=do_job)
edit_menu.add_cascade(label='Paste', command=do_job)

submenu = tk.Menu(file_menu)
file_menu.add_cascade(label='import', menu=submenu,
                     underline=0)
submenu.add_command(label='Submenu', command=do_job)

root.config(menu=menu)
root.mainloop()     # 大型的while循环

