"""
时间：2021/03/10
版本号：1.7
重点：Checkbutton勾选项
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("500x400")

l = tk.Label(root,bg='green',width=25,
             text="empty")
l.pack()

def print_selection():
    if (var1.get()==1)&(var2.get()==0):
        l.config(text='I love only Python')
    elif (var1.get()==0)&(var2.get()==1):
        l.config(text="I love only C++")
    elif (var1.get()==0)&(var2.get()==0):
        l.config(text="I don't love either")
    else:
        l.config(text='I love both')

var1 = tk.IntVar()
var2 = tk.IntVar()
cb1 = tk.Checkbutton(root,text='Python',variable=var1,
                     onvalue=1,offvalue=0,
                     command=print_selection)
cb1.pack()
cb2 = tk.Checkbutton(root,text="C++",variable=var2,
                     onvalue=1,offvalue=0,
                     command=print_selection)
cb2.pack()

root.mainloop()     # 大型的while循环



