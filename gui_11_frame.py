"""
时间：2021/03/10
版本号：2.1
重点：Frame框架
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("300x200")

tk.Label(root,text='on the windows').pack()

frm = tk.Frame(root)
frm.pack()
frm_l = tk.Frame(frm)
frm_r = tk.Frame(frm)
frm_l.pack(side='left')
frm_r.pack(side='right')

tk.Label(frm_l,text='on the frm_l1').pack()
tk.Label(frm_l,text='on the frm_l2').pack()
tk.Label(frm_r,text='on the frm_rl').pack()


root.mainloop()     # 大型的while循环

