"""
时间：2021/03/11
版本号：2.5
重点：登录窗口2
"""
import tkinter as tk
import tkinter.messagebox
import pickle

root = tk.Tk()  # 实例化对象
root.title('Welcome to Python')     # 给窗口取名
root.geometry("400x300")


canvas = tk.Canvas(root, height=200, width=500)
image_file = tk.PhotoImage(file='welcome.gif')
image = canvas.create_image(35, 0, anchor='nw',
                            image=image_file)
canvas.pack(side='top')

tk.Label(root, text='User name:', font=('Arial', 14)).place(x=45, y=160)
tk.Label(root, text='Password:', font=('Arial', 14)).place(x=45, y=190)

var_user_name = tk.StringVar()
var_user_name.set('python123@gmail.com')

entry_user_name = tk.Entry(root, textvariable=var_user_name)
entry_user_name.place(x=160, y=160)

var_user_password = tk.StringVar()
entry_user_password = tk.Entry(root, textvariable=var_user_password, show='*')
entry_user_password.place(x=160, y=190)


def user_login():
    user_name = var_user_name.get()
    user_pssword = var_user_password.get()
    try:
        with open("usrs_info.pickle",'rb') as user_file:
            usrs_info = pickle.load(user_file)
    except FileNotFoundError:
        with open('usrs_info.pickle', 'wb') as user_file:
            usrs_info={'admin': 'admin'}
            pickle.dump(usrs_info, user_file)

    if user_name in usrs_info:
        if user_pssword == usrs_info[user_name]:
            tk.messagebox.showinfo(title="Welcome", message='How are you?' + user_name)
        else:
            tk.messagebox.showerror(message="Error your password is wrong,try again!")
    else:
        is_sign_up = tk.messagebox.askyesno(title='Welcome', message='You have not sign up yet,sign up today?')

        if is_sign_up:
            user_sign_up()



def user_sign_up():
    pass


login = tk.Button(root, text='Login', command=user_login).place(x=170, y=210)
sign_up = tk.Button(root, text='Sign up', command=user_sign_up).place(x=240, y=210)

root.mainloop()     # 大型的while循环

