"""
时间：2021/03/10
版本号：2.2
重点：messagebox 弹窗
"""
import tkinter as tk
import tkinter.messagebox

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("300x200")


def hit():
    tkinter.messagebox.showinfo(title='Hi', message='你好')
    tkinter.messagebox.showwarning(title='waring', message='警告')
    tkinter.messagebox.showerror(title='error', message='有错误')
    tkinter.messagebox.askquestion(title='Hi', message='问题')


tk.Button(root, text='hit', width=10,
          height=2, command=hit).pack()
root.mainloop()     # 大型的while循环

