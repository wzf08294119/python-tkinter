"""
时间：2021/03/10
版本号：1.8
重点：Canvas画布
"""
import tkinter as tk

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("300x200")

canvas = tk.Canvas(root,bg='blue',height=100,width=200)

image_file = tk.PhotoImage(file='le.gif')
image = canvas.create_image(30,30,anchor='nw',
                        image=image_file)

x0,y0,x1,y1=50,50,80,80
line = canvas.create_line(x0,y0,x1,y1)
oval = canvas.create_oval(x0,y0,x1,y1,fill='red')
canvas.pack()

def moveit():
    canvas.move(oval,2,2)
b = tk.Button(root,text='move',command=moveit).pack()

root.mainloop()     # 大型的while循环



