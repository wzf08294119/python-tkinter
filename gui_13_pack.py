"""
时间：2021/03/10
版本号：2.3
重点：pack(),grid(),place()
"""
import tkinter as tk
import tkinter.messagebox

root = tk.Tk()  # 实例化对象
root.title('title')     # 给窗口取名
root.geometry("300x200")

# tk.Label(root,text='Hello').pack(side='top')
# tk.Label(root,text='Hello').pack(side='bottom')
# tk.Label(root,text='Hello').pack(side='left')
# tk.Label(root,text='Hello').pack(side='right')

# for i in range(4):
#     for j in range(3):
#         tk.Label(root,text='Hi').grid(
#             row=i,column=j,padx=10,pady=10)

tk.Label(root,text='jean').place(x=20,y=20,anchor='nw')


root.mainloop()     # 大型的while循环

